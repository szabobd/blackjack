import Felgo 3.0
import QtQuick 2.0

App {

    property int dealerNums: 0 //12 is the min where dealer might choose not to hit - should not be visible
    property int currentSum: 0

    readonly property real contentPadding: dp(Theme.navigationBar.defaultBarItemPadding)

    NavigationStack {
        id: navStack


        Page {
            id: startPage
            backgroundColor: "green"
            backNavigationEnabled: false
            navigationBarTranslucency: 1

            AppButton {
                text: "Start game"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                flat: false
                backgroundColor: "white"
                onClicked: {
                    startPage.navigationStack.push(gamePage)
                    startSum()
                    dealerNumMaker()

                }
            }
        }

    }
    Component {
        id: gamePage


        Page {
            title: "Black Jack"
            backNavigationEnabled: false
            navigationBarTranslucency: 1

            Column {
                id: contentCol
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.margins: contentPadding
                spacing: contentPadding

                Text {
                    text: "The dealer has %1".arg(dealerNums) //animate the reveal, delay before pushing endpage
                }

                Text {
                    text: "You have %1".arg(currentSum)
                }

                Row {
                    spacing: contentPadding

                    AppButton {
                        text: "Hit"
                        onClicked: hitNew()



                    }

                    AppButton {
                        text: "Done"
                        onClicked: didYouWin()


                    }

                }

            }

        }
    }

    function startSum() {
        var startNums = utils.generateRandomValueBetween(2, 20)
        currentSum += startNums
    }

    function dealerNumMaker() {
        var dealerSum = utils.generateRandomValueBetween(12, 21)
        dealerNums += dealerSum
    }

    function hitNew () {
        var randNums = utils.generateRandomValueBetween(2, 10)
        currentSum += randNums
        if (currentSum > 21) {
            navStack.push(losePage)
        }
    }

    Component {
        id: winPage

        Page {
            backNavigationEnabled: false
            navigationBarTranslucency: 1


            Column {
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.margins: contentPadding
                spacing: contentPadding

                Text {
                    text: "You win! You have %1 and the dealer has %2".arg(currentSum).arg(dealerNums)
                }
            }

        }
    }

    Component {
        id: losePage

        Page {

            backNavigationEnabled: false
            navigationBarTranslucency: 1

            Column {
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.margins: contentPadding
                spacing: contentPadding

                Text {
                    text: "You lose! You have %1 and the dealer has %2".arg(currentSum).arg(dealerNums)
                }

                AppButton {
                    text: "Back to the start"
                    onClicked: navStack.popAllExceptFirstAndPush(startPage), currentSum = 0, dealerNums = 0
                }
            }
        }
    }

    function didYouWin() {
        if (currentSum > dealerNums){
            navStack.push(winPage)
        } else {
            navStack.push(losePage)
        }
    }

}
